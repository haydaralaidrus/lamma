#include <stdlib.h>
#include "lamma.h"

note_sequence_t *
note_sequence_new(void)
{
	note_sequence_t *note_sequence;

	note_sequence = malloc(sizeof(note_sequence_t));
	if (note_sequence == NULL)
		return NULL;

	note_sequence->nsequence = 0U;
	note_sequence->nrepeat = 0U;

	return note_sequence;
}

void
note_sequence_free(note_sequence_t **note_sequence)
{
	if (*note_sequence == NULL)
		return;

	free(*note_sequence);
	*note_sequence = NULL;
}

int
note_sequence_add_note(note_sequence_t **note_sequence, const freq_t freq,
                       const unsigned int repeat)
{
	void *tmp;

	if (*note_sequence == NULL || repeat == 0U)
		return -1;

	tmp = realloc(
		*note_sequence,
		sizeof(note_sequence_t) + (
			sizeof(note_t) * ((*note_sequence)->nsequence + 1)
		)
	);

	if (tmp == NULL)
		return -1;

	*note_sequence = tmp;
	(*note_sequence)->notes[(*note_sequence)->nsequence].freq = freq;
	(*note_sequence)->notes[(*note_sequence)->nsequence].repeat = repeat;
	(*note_sequence)->nsequence += 1U;
	(*note_sequence)->nrepeat += repeat;

	return 0;
}
