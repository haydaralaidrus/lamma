#ifndef LAMMA_H
#define LAMMA_H

#include <math.h>

#ifndef M_PI
#define M_PI (3.14159265358979323846264338)
#endif

#define attack(A, T, D) ((A / D) * T)

#define decay(A, T, D) (A - ((A * T) / (2.0 * D)))

#define suspend(A) (A / 2.0)

#define release(A, T, D) ((A / 2.0) + ((-A / 2.0) / D) * T)

#define drop(A, T, D) (A + (-A / D) * T)

#define sinusoid(A, F, T, R) (A * sin((F / R) * 2.0 * T * M_PI))

#define sawtooth(A, F, T, R) ((2.0 * A / M_PI) * \
                              atan(tan((F / R) * M_PI * T)))

#define triangle(A, F, T, R) ((2.0 * A / M_PI) * \
                              asin(sin((F / R) * 2.0 * M_PI * T)))

#define square(A, F, T, R) (sin((F / R) * 2.0 * M_PI * T) >= 0 ? A : -A)

#define pulse(A, F, T, R, D) ((T - floor(T * F / R) * R / F) < D * R / F ? A \
                              : -A)

#define ramp(A, F, T, R) (2.0 * A * ((T * F / R) - floor(0.5 + T * F / R)))

#define noise(A) (A * ((2.0 * rand() / RAND_MAX) - 1.0))

typedef double freq_t;

typedef struct note {
	freq_t       freq;
	unsigned int repeat;
} note_t;

typedef struct note_sequence {
	unsigned int nsequence;
	unsigned int nrepeat;
	note_t       notes[0];
} note_sequence_t;

note_sequence_t *
note_sequence_new(void);

void
note_sequence_free(note_sequence_t **note_sequence);

int
note_sequence_add_note(note_sequence_t **note_sequence, const freq_t freq,
                       const unsigned int repeat);

#endif
